'use strict';

angular.module('app', [
        'ngAnimate',
		'app.config',
        'angular-loading-bar',
        'ngStorage',
        'ui.router',
        'ui.bootstrap',
        'ngSanitize',
        'ui.gravatar',
        'restangular',
		'app.directives',
		'app.filters',
		'app.services',
		'app.modules'
    ])
	.run(['$rootScope', '$state', '$stateParams', 'Auth', '$location', '$window', '$httpBackend',
		function ($rootScope, $state, $stateParams, Auth, $location, $window, $httpBackend) {
			$rootScope.$state = $state;
			$rootScope.$stateParams = $stateParams;
			$rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
				var user = Auth.getUser();
				if (user) {
					if (Date.create(user.exp * 1000).isPast()) {
						event.preventDefault();
						Auth.logout();
						$state.go('access.login');
					}
				}

				if (!Auth.isAuthenticated() && !toState.name.includes('access', 0)) {
					event.preventDefault();
					$state.go('access.login');
				}

				if (toState.name == 'access.login' && Auth.isAuthenticated()) {
					event.preventDefault();
					$state.go('dashboard');
				}


				// If it's a parent state, redirect to it's child
				if (toState.redirectTo) {
					event.preventDefault();
					var params = toParams;
					if (!_.isEmpty(fromParams)) _.extend(toParams, $location.search());
					$state.go(toState.redirectTo, params);
					return;
				}

			});
		}
	])
	.config(['$stateProvider', '$urlRouterProvider', 'RestangularProvider', 'Config', function($stateProvider, $urlRouterProvider, RestangularProvider, config, $mdThemingProvider){
	    RestangularProvider.addResponseInterceptor(function(data, operation, what, url, response, deferred) {
	        if (data.response && data.response.data) {
	            var returnedData = data.response.data;
	            return returnedData;
	        } else {
	            return data;
	        };
	    });
		$urlRouterProvider.when('', '/login');
	    $urlRouterProvider.otherwise('/');
	}])
	.controller('AppCtrl', function ($scope, $window, $state, Auth, $rootScope, $log, $timeout, $localStorage) {
		$scope.user = Auth.getUser();
        $scope.$on('fetchUserData', function (event, data) {
            $scope.user = Auth.getUser();
        });
		$scope.app = {
            name: 'Admin',
            version: '1.0.0',
			settings: {
                themeID: 5,
                navbarHeaderColor:"bg-success",
				navbarCollapseColor:"bg-white-only",
				asideColor:"bg-dark",
				headerFixed:false,
				asideFixed:false,
				asideFolded:false,
				asideDock:false,
				container:false
            }
        }

        // save settings to local storage
        if (angular.isDefined($localStorage.AdminSettings)) {
            $scope.app.settings = $localStorage.AdminSettings;
        } else {
            $localStorage.AdminSettings = $scope.app.settings;
        }
        $scope.$watch('app.settings', function() {
            if ($scope.app.settings.asideDock && $scope.app.settings.asideFixed) {
                // aside dock and fixed must set the header fixed.
                $scope.app.settings.headerFixed = true;
            }
            // save to local storage
            $localStorage.AdminSettings = $scope.app.settings;
        }, true);

        $scope.logout = function () {
            Auth.logout();
            window.location.reload();
            // $state.go('landing');
        }

		// add 'ie' classes to html
		var isIE = !!navigator.userAgent.match(/MSIE/i);
		isIE && angular.element($window.document.body).addClass('ie');
		isSmartDevice($window) && angular.element($window.document.body).addClass('smart');
		function isSmartDevice($window) {
			// Adapted from http://www.detectmobilebrowsers.com
			var ua = $window['navigator']['userAgent'] || $window['navigator']['vendor'] || $window['opera'];
			// Checks for iOs, Android, Blackberry, Opera Mini, and Windows mobile devices
			return (/iPhone|iPod|iPad|Silk|Android|BlackBerry|Opera Mini|IEMobile/).test(ua);
		}
	})


angular.module('ui.gravatar').config(['gravatarServiceProvider',
    function(gravatarServiceProvider) {
        gravatarServiceProvider.defaults = {
            size: 100,
            "default": 'mm' // Mystery man as default for missing avatars
        };

        // Use https endpoint
        // gravatarServiceProvider.secure = true;
    }
]);
