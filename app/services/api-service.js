angular.module('app.apiService', [])
.factory('API', ['Restangular', 'Config', function(Restangular, settings) {
    return Restangular.withConfig(function(RestangularConfigurer) {
        RestangularConfigurer.setBaseUrl(settings.api);
    });
}]);
