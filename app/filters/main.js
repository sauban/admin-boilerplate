angular.module('app.filters', [])
.filter("sanitize", ['$sce', function($sce) {
  return function(htmlCode){
    return $sce.trustAsHtml(htmlCode);
  }
}])
.filter('naira', ['$filter', '$locale',
        function(filter, locale) {
            var currencyFilter = filter('currency');
            var formats = locale.NUMBER_FORMATS;
            return function(amount, currencySymbol) {
                amount = amount / 100;
                var value = currencyFilter(amount, "₦");
                var sep = value.indexOf(formats.DECIMAL_SEP);
                if (amount >= 0) {
                    return value.substring(0, sep);
                }
                return value.substring(0, sep) + '';
            };
        }
])
.filter('titleCase', function() {
        return function (data){
            if(!data){
                return '';
            };
            if (Array.isArray(data)) {
                return data.map(function(val){
                    return capitalize(val);
                })
            }
            function capitalize(str){
                return str.replace(/\w\S*/g,
                    function(txt){
                        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                    });
            }
            return capitalize(data);
        }
})
.filter('typeFormat', function() {
        return function (type){
            if(!type){
                return '';
            };
            if(type === 'student'){ return 'Student History'; }
            if (type === 'board') { return 'Board Certification' }
            if (type === 'employee') { return 'Employee History' }
            return 'other';
        }
})
.filter('decamelize', function () {
  return function (str) {
    var sp = str.replace(/(client|saas)/g, '').split('');
    for(var i=0; i<sp.length; i++){
        var l = sp[i];
        if(i == 0) sp[i] = l.toUpperCase();
        if(l.match(/[A-Z]/)){
            sp[i] = ' ' + l;
        }
    };
    return sp.join('');
  }
})
.filter('monthName', function() {
        return function (number){
            if(!number){
                return '';
            };
            var index = +number;
            var months = [
                'January',
                'Febuary',
                'March',
                'April',
                'May',
                'June',
                'July',
                'August',
                'September',
                'October',
                'November',
                'December'
            ];
            return months[--index];
        }
})
