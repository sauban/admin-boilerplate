angular.module('app.directives', ['ui.jq', 'ui.directives'])
.directive('type', ['$parse', function urModelFileFactory($parse) {

  /**
   * Binding for file input elements
   */
  return {
	scope: false,
	priority: 1,
	require: "?ngModel",
	link: function urFilePostLink(scope, element, attrs, ngModel) {

	  if (attrs.type.toLowerCase() !== 'file' || !ngModel) {
		return;
	  }

	  element.bind('change', function(e) {
		if (!e.target.files || !e.target.files.length || !e.target.files[0]) {
		  return true;
		}
		var index, fileData = attrs.multiple ? e.target.files : e.target.files[0];
		ngModel.$render = function() {};

		scope.$apply(function(scope) {
		  index = scope.$index;
		  $parse(attrs.ngModel).assign(scope, fileData);
		});
		scope.$index = index;

		// @todo Make sure this can be replaced by ngChange.
		// For that to work, this event handler must have a higher priority than the one
		// defined by ngChange
		attrs.change ? scope.$eval(attrs.change) : null;
	  });
	}
  };

}])
.directive('fileField', function() {
      return {
        require:'ngModel',
        restrict: 'AE',
        link: function (scope, element, attrs, ngModel) {
            //set default bootstrap class
            if(!attrs.class && !attrs.ngClass){
                element.addClass('btn large full');
            }

            var fileField = element.find('input');
            fileField.attr('accept', attrs.accept)

            fileField.bind('change', function(event){
                scope.$evalAsync(function () {
                  ngModel.$setViewValue(event.target.files[0]);
                  if(attrs.preview){
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        scope.$evalAsync(function(){
                            scope[attrs.preview]=e.target.result;
                        });
                    };
                    if (event.target.files[0]) {
                        reader.readAsDataURL(event.target.files[0]);
                    }
                  }
                });
            });
            fileField.bind('click',function(e){
                e.stopPropagation();
            });
            element.bind('click',function(e){
                e.preventDefault();
                fileField.click()
            });
        },
        template:'<a><ng-transclude></ng-transclude><input type="file" name="file" style="display:none"></a>',
        replace:true,
        transclude:true
      };
});
