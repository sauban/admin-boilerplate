var secretEmptyKey = '[$empty$]';
angular.module('ui.directives', [])
      .directive('ngContactMobile', ['$window', 'Notification', function($window, Notification) {
          //directive to show loading state
          return {
              restrict: 'AE',
              scope: true,
              compile: function(tElem, attrs) {
                  //Add the controls to element
                  return function(scope, element, attrs) {
                      var type = attrs.ngContactMobile;
                      var isMobile = {
                          Android: function() {
                              return navigator.userAgent.match(/Android/i);
                          },
                          BlackBerry: function() {
                              return navigator.userAgent.match(/BlackBerry/i);
                          },
                          iOS: function() {
                              return navigator.userAgent.match(/iPhone|iPad|iPod/i);
                          },
                          Opera: function() {
                              return navigator.userAgent.match(/Opera Mini/i);
                          },
                          Windows: function() {
                              return navigator.userAgent.match(/IEMobile/i);
                          },
                          any: function() {
                              return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
                          }
                      };
                      function loadMobile() {
                          if(isMobile.Android() && attrs.ngActionType == 'biometrics'){
                              $window.verifiInterface.getFingerprint(type);
                          }
                          if(isMobile.Android() && attrs.ngActionType == 'signature'){
                              $window.verifiInterface.getSignature();
                          }
                          if(isMobile.iOS() && attrs.ngActionType == 'biometrics'){
                            var postObject = {
                                body: {
                                    signature: signatureDoc.id
                                }
                            };
                            $window.webkit.messageHandlers.onEnroll.postMessage(postObject);
                          }
                          if(isMobile.iOS() && attrs.ngActionType == 'signature'){
                            var postObject = {
                                body: {
                                    signature: signatureDoc.id
                                }
                            };
                            $window.webkit.messageHandlers.onEnroll.postMessage(postObject);
                          }
                      };

                      scope.$on(attrs.ngContactMobile, function(e, val) {
                          if (val == true) element.addClass('capture-success');
                          else element.addClass('capture-failure');
                      });

                      element.on('click', function() {
                          loadMobile();
                      })
                  };
              }
          };
      }])
    .directive('hamburger', function () {
        return {
          restrict: 'C',
          scope: true,
          compile: function (tElem, attrs) {
            return function ($scope, element) {
              element.on('click', function (e) {
                var body = angular.element(document).find('body'),
                    sidebar = body.find('.app-aside'),
                    parent = $(this).parent();
                // e.stopPropagation();
                // console.log(e.currentTarget.parent);
                //move the element right.

                if (!sidebar.hasClass('off-screen')) {
                  sidebar.addClass('off-screen');
                  parent[0].style["margin-left"] = sidebar.width() + 'px';
                  e.currentTarget.style["margin-left"] = sidebar.width() + 'px';
                }
                else {
                  body.find('div.child_menu').addClass('ng-hide');
                  sidebar.removeClass('off-screen');
                  parent[0].style["margin-left"] = '0px';
                  e.currentTarget.style["margin-left"] = '0px';
                }

              })
            };
          }
        }
      })
    .directive('naviroute', function () {
      return {
        restrict: 'A',
        scope: true,
        compile: function (tElem, attrs) {
          var body = angular.element(document).find('body');
          return function ($scope, element) {
            var child_elem = body.find('div#' + attrs["navId"] + '.child_menu');
            body.find('div.child_menu').addClass('ng-hide');
            element.on('click', function (e) {
              e.stopPropagation();
              body.find('div.child_menu').not(child_elem).addClass('ng-hide');
              child_elem.toggleClass('ng-hide');
              child_elem[0].style.top = (e.pageY - e.offsetY) + 'px';
            })
          };
        }
      }
    })
    .directive('badge', function() {
      return {
          restrict: 'E',
          replace: true,
          template: '<span class="badge text-capitalize" ng-class="label_class">{{status}}</span>',
          link: function($scope, element, attrs, controller) {
              $scope.status = attrs["status"] || "Unknown";
              $scope.label_class = attrs["class"] || "";

              var success_states = ['verified', 'paid', 'active', 'success', 'subscribed'];
              var failure_states = ['failed', 'unpaid', 'inactive', 'unverified', 'unsubscribed', 'expired', 'cancelled', 'Cancelled'];

              if (_.contains(success_states, $scope.status)) {
                  $scope.label_class += " btn-success"
              } else if (_.contains(failure_states, $scope.status)) {
                  $scope.label_class += " btn-danger"
              } else if ($scope.status == 'pending') {
                  $scope.label_class += " btn-clear no-text-shadow"
              } else {
                  $scope.label_class += " btn-info"
              }
          }
      };
    })
    .directive('avatar', function() {
        return {
            restrict: 'E',
            replace: true,
            template: '<span class="user-avatar" ng-style="style">{{initials}}</span>',
            link: function($scope, element, attrs, controller) {
                var colors = ['#3676C8', '#3676C8', '#2c3e50', '#27ae60', '#16a085']
                var name = attrs["name"] || "--";
                if (name.length > 1) _name = name.split(" ")[0].charAt(0).toUpperCase() + name.split(" ")[1].charAt(0).toUpperCase();
                else _name = name.substring(0, 2);
                $scope.initials = _name;
                $scope.style = {
                    'background-color': _.sample(colors)
                }
            }
        };
    })
    .directive('ngLoading', [function() {
        //directive to show loading state
        return {
            restrict: 'AE',
            scope: true,
            compile: function(tElem, attrs) {
                //Add the controls to element
                tElem.addClass('loading-button');
                var buttonContent = tElem.html();
                tElem.html("<span class=\"default-state\">" + buttonContent + "</span>");
                tElem.append("<div class=\"spinner\">\r\n  <div class=\"b1 se\"><\/div>\r\n  <div class=\"b2 se\"><\/div>\r\n  <div class=\"b3 se\"><\/div>\r\n  <div class=\"b4 se\"><\/div>\r\n  <div class=\"b5 se\"><\/div>\r\n  <div class=\"b6 se\"><\/div>\r\n  <div class=\"b7 se\"><\/div>\r\n  <div class=\"b8 se\"><\/div>\r\n  <div class=\"b9 se\"><\/div>\r\n  <div class=\"b10 se\"><\/div>\r\n  <div class=\"b11 se\"><\/div>\r\n  <div class=\"b12 se\"><\/div>\r\n<\/div><span class=\"loading-success\"><i class=\"fa fa-check animated fadeInUp\"><\/i><\/span><span class=\"loading-failure\"><i class=\"fa fa-times animated fadeInUp\"><\/i><\/span>");
                return function(scope, element, attrs) {
                    var watching;
                    var load = function(val) {
                        element.addClass('ng-loading');
                        element.attr('disabled', true);
                        if (attrs.loadingText) {
                            buttonContent = attrs.loadingText;
                        }
                        watching = true;
                    }
                    scope.$on(attrs.ngLoading, function(e, val) {
                        if (!watching) return;
                        watching = false;
                        element.removeClass('ng-loading');
                        if (val == true) element.addClass('ng-loading-success')
                        else element.addClass('ng-loading-failure');
                        setTimeout(function() {
                            element.removeClass('ng-loading-success ng-loading-failure ng-loading');
                            element.attr('disabled', false);
                        }, 700);
                    });
                    element.on('click', function() {
                        element.addClass('ng-loading');
                        load();
                    })
                };
            }
        };
    }])
    .directive('focus', ['$timeout',
        function($timeout) {
            //directive to focus on on input
            return {
                scope: {
                    trigger: '@focus'
                },
                link: function(scope, element) {
                    scope.$watch('trigger', function(value) {
                        if (value === "true") {
                            $timeout(function() {
                                element[0].focus();
                            });
                        }
                    });
                }
            };
        }
    ])
    .filter('myLimitTo', function() {
        return function(input, limit, begin) {
            return input ? input.slice(begin, begin + limit) : "";
        };
    })
    .directive('ngEnter', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if(event.which === 13) {
                    scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });
                    event.preventDefault();
                }
            })
        };
    })
    .directive('focusMe', function($timeout, $parse) {
      return {
          //scope: true,   // optionally create a child scope
          link: function(scope, element, attrs) {
              var model = $parse(attrs.focusMe);
              scope.$watch(model, function(value) {
                  if(value === true) {
                      $timeout(function() {
                          element[0].focus();
                      });
                  }
              });
          }
      };
    })
    .directive('emptyTypeahead', function () {
      return {
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
          // this parser run before typeahead's parser
          modelCtrl.$parsers.unshift(function (inputValue) {
            var value = (inputValue ? inputValue : secretEmptyKey); // replace empty string with secretEmptyKey to bypass typeahead-min-length check
            modelCtrl.$viewValue = value; // this $viewValue must match the inputValue pass to typehead directive
            return value;
          });

          // this parser run after typeahead's parser
          modelCtrl.$parsers.push(function (inputValue) {
            return inputValue === secretEmptyKey ? '' : inputValue; // set the secretEmptyKey back to empty string
          });
        }
      }
    })
    .directive('typeaheadFocus', function () {
      return {
        require: 'ngModel',
        link: function (scope, element, attr, ngModel) {

          //trigger the popup on 'click' because 'focus'
          //is also triggered after the item selection
          element.bind('click', function () {

            var viewValue = ngModel.$viewValue;

            //restore to null value so that the typeahead can detect a change
            if (ngModel.$viewValue == ' ') {
              ngModel.$setViewValue(null);
            }

            //force trigger the popup
            ngModel.$setViewValue(' ');

            //set the actual value in case there was already a value in the input
            ngModel.$setViewValue(viewValue || ' ');
          });

          //compare function that treats the empty space as a match
          scope.emptyOrMatch = function (actual, expected) {
            if (expected == ' ') {
              return true;
            }
            return actual.indexOf(expected) > -1;
          };
        }
      };
    }).filter('propsFilter', function() {
        return function(items, props) {
          var out = [];

          if (angular.isArray(items)) {
            var keys = Object.keys(props);

            items.forEach(function(item) {
              var itemMatches = false;

              for (var i = 0; i < keys.length; i++) {
                var prop = keys[i];
                var text = props[prop].toLowerCase();
                if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                  itemMatches = true;
                  break;
                }
              }

              if (itemMatches) {
                out.push(item);
              }
            });
          } else {
            // Let the output be the input untouched
            out = items;
          }

          return out;
        };
      });
