'use strict';
angular.module('app.user', [
    'ui.router'
  ])
  .config(['$stateProvider',
	  function ($stateProvider) {
	      $stateProvider
	      .state('user', {
	          url: '/users',
	          templateUrl: 'components/layouts/default.html',
			  redirectTo: 'user.home'
	      })
		  .state('user.home', {
			  url: '',
			  templateUrl: 'modules/user/list.html'
		  })
	  }
])
