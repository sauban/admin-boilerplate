'use strict';
angular.module('app.dashboard', [
    'ui.router'
  ])
  .config(['$stateProvider',
	  function ($stateProvider) {
	      $stateProvider
	      .state('dashboard', {
	          url: '/',
	          templateUrl: 'components/layouts/default.html',
			  redirectTo: 'dashboard.home'
	      })
		  .state('dashboard.home', {
			  url: '',
			  templateUrl: 'modules/dashboard/index.html'
		  })
	  }
])
.controller('LeftCtrl', function ($scope, $timeout, $mdSidenav, $log) {
    $scope.close = function () {
      // Component lookup should always be available since we are not using `ng-if`
      $mdSidenav('left').close()
        .then(function () {
          $log.debug("close LEFT is done");
        });

    };
})
.controller('RightCtrl', function ($scope, $timeout, $mdSidenav, $log) {
$scope.close = function () {
  // Component lookup should always be available since we are not using `ng-if`
  $mdSidenav('right').close()
    .then(function () {
      $log.debug("close RIGHT is done");
    });
};
});
