'use strict';
angular.module('app.organization', [
    'ui.router'
])
.config(['$stateProvider',
	  function ($stateProvider) {
	      $stateProvider
	      .state('organization', {
	          url: '/organizations',
	          templateUrl: 'components/layouts/default.html',
			  redirectTo: 'organization.home',
			  resolve: {
				  organizations: function (API, $stateParams) {
				  	return API.all('organization').getList($stateParams);
				  }
			  }
	      })
		  .state('organization.home', {
			  url: '',
			  templateUrl: 'modules/organization/list.html',
			  controller: 'organizationCtrl'
		  })
		  .state('organization.one', {
			  url: '/:id',
			  templateUrl: 'modules/organization/one.html',
			  resolve: {
				organization: function (API, organizations, $stateParams) {
					// var organization = organizations.find(function (organization) {
					// 	return organization.id == $stateParams.id;
					// })
					var organization = _.findWhere(organizations.plain(), {id: +($stateParams.id)});
					return organization ? organization : API.one('organization', $stateParams.id).get();
				}
			  },
			  controller: 'organizationSingleCtrl'
		  })
	  }
])
.controller('organizationCtrl', function ($scope, API, organizations) {
	$scope.organizations = organizations;
})
.controller('organizationSingleCtrl', function ($scope, API, organization) {
	$scope.organization = organization;
})
