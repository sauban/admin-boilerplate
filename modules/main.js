angular.module('app.modules', [
	'app.dashboard',
	'app.access',
	'app.organization',
	'app.store',
	'app.user'
]);
