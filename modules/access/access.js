'use strict';
angular.module('app.access', ['ui.router'])
  .config(['$stateProvider',
    function ($stateProvider) {
      $stateProvider
      .state('access', {
          abstract: true,
          url: '',
          templateUrl: 'modules/access/index.html',
      })
      .state('access.login', {
          url: '/login',
          templateUrl: 'modules/access/login.html',
          controller: 'LoginCtrl'
      })
      .state('access.signup', {
          url: '/signup',
          templateUrl: 'modules/access/signup.html',
          controller: 'LoginCtrl'
      })
  }
  ])


  .controller('LoginCtrl', ['$scope', '$state', 'Auth', 'VarService', 'API','$rootScope',
      function ($scope, $state, Auth, VarService, API, $rootScope) {
          $scope.credentials = {};
          $scope.login = function () {
              Auth.login($scope.credentials).then(function (res) {
                  $scope.$emit('fetchUserData', 'true');
                      $state.go('dashboard.home');
              }, function (error) {
                  $scope.alert = {
                      type: 'danger',
                      message: error.data.response.message
                  };
              });
          };
      }
  ])
