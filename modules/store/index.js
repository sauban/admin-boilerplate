'use strict';
angular.module('app.store', [
    'ui.router'
  ])
  .config(['$stateProvider',
	  function ($stateProvider) {
	      $stateProvider
	      .state('store', {
	          url: '/stores',
	          templateUrl: 'components/layouts/default.html',
			  redirectTo: 'store.home'
	      })
		  .state('store.home', {
			  url: '',
			  templateUrl: 'modules/store/list.html'
		  })
	  }
])
