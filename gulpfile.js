var gulp = require('gulp'),
    connect = require('gulp-connect'),
    clean = require('gulp-clean'),
    cssmin = require('gulp-cssmin'),
    rename = require('gulp-rename'),
    less = require('gulp-less'),
    concat = require('gulp-concat'),
    inject = require('gulp-inject'),
    fs = require('fs'),
    argv = require('yargs').argv,
    gulpif = require('gulp-if'),
    uglify = require('gulp-uglify'),
    gulpNgConfig = require('gulp-ng-config'),
    filter = require('gulp-filter'),
    json = JSON.parse(fs.readFileSync('./package.json')),
	purify = require('gulp-purifycss');


gulp.task('clean', function () {
  return gulp.src('dist/**/*')
    .pipe(filter(['*', '!.git']))
    .pipe(clean());
});

gulp.task('config', function () {
  gulp.src('./local.json')
  .pipe(gulpNgConfig('app.config', {
    environment: argv.production ? 'production' : 'development'
  }))
  .pipe(gulp.dest('app'))
})

gulp.task('api', ['clean'], function () {
  gulp.src('./mocks/*')
  .pipe(gulp.dest('dist/api'))
})

gulp.task('connect', function(){
    connect.server({
        root: 'dist',
        port: 4000
    });
});

gulp.task('copy-html', ['clean'], function(){
    return gulp.src(['app/index.html', './**/*.html', '!node_modules/**/*.html', '!vendor/**/*.html', '!dist/**/*.html'])
            .pipe(gulp.dest('dist'));
});

gulp.task('copy-fonts', ['clean'], function(){
    return gulp.src('vendor/fonts/**/*')
            .pipe(gulp.dest('dist/assets/fonts'));
});

gulp.task('copy-images', ['clean'], function(){
    return gulp.src('vendor/images/**/*')
            .pipe(gulp.dest('dist/assets/images'));
});

gulp.task('vendorjs', ['clean'], function(){
    return gulp
		.src([
		'vendor/js/jquery.min.js',
		'vendor/js/angular.js',
		'vendor/js/angular-animate.min.js',
		'vendor/js/angular-aria.min.js',
		'vendor/js/*.js'
		])
        .pipe(concat('vendor.js'))
        .pipe(gulpif(argv.production, uglify()))
        .pipe(gulp.dest('dist/assets/js'));
});

gulp.task('vendorcss', ['clean'], function(){
    return gulp
			.src([
				'vendor/css/font-awesome.min.css',
				'vendor/css/bootstrap.css',
				'vendor/**/*.css'
			])
		    .pipe(concat('vendor.css'))
		    .pipe(gulpif(argv.production, cssmin()))
			.pipe(gulpif(argv.production, purify(['./dist/**/*.js', './dist/**/*.html'])))
		    .pipe(gulp.dest('dist/assets/css'))
});

gulp.task('appjsfiles', ['clean'], function(){
    return gulp.src([ 'app/local.js', 'app/app.js', 'app/filters/*.js', 'app/services/*.js', 'app/directives/*.js', 'modules/**/*.js'])
            .pipe(concat(json.name.toLowerCase() + '.js'))
            .pipe(gulpif(argv.production, uglify()))
            .pipe(gulpif(argv.production, rename({suffix: '.min'})))
            .pipe(gulp.dest('dist/assets/js'));
});

gulp.task('link-files', ['copy-images', 'copy-fonts', 'copy-html',  'vendorjs', 'vendorcss', 'appjsfiles', 'api'], function () {
  var target = gulp.src('dist/index.html');
  var sources = gulp.src(['dist/assets/js/vendor.js', 'dist/assets/js/*.js', 'dist/assets/css/vendor.css', 'dist/assets/css/*.css'], {read: false});
  return target.pipe(inject(sources, {relative: true}))
    .pipe(gulp.dest('dist'));
});

gulp.task('style', ['clean'], function(){
    return gulp.src('styles/app.less')
        .pipe(less())
        .pipe(rename(json.name.toLowerCase() + '.css'))
        .pipe(gulpif(argv.production, cssmin()))
		.pipe(gulpif(argv.production, purify(['./dist/**/*.js', './dist/**/*.html'])))
        .pipe(gulpif(argv.production, rename({suffix: '.min'})))
		.pipe(gulp.dest('dist/assets/css'))
});

gulp.task('logger', function(){
    console.info('===== A file changed ======== restarting server now ====');
});

gulp.task('build', [ 'config', 'style', 'link-files']);

gulp.task('serve', ['connect', 'config', 'style', 'link-files'], function () {
	return gulp.watch(['app/**/*', 'modules/**/*', 'components/**/*', 'styles/**/*'], ['logger', 'style', 'link-files']);
});
